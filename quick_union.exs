defmodule QuickUnion do
  def create(count) do
    last = count - 1
    Enum.into(0..last, %{}, fn(x) -> {x, x} end)
  end

  def connected?(data, q, p) do
    root(data, q) == root(data, p)
  end

  def union(data, q, p) do
   root_q = root(data, q)
   root_p = root(data, p)
   Map.put(data, root_q, root_p)
  end

  def root(data, n) do
    if data[n] == n do
      n
    else
      root(data, data[n])
    end
  end 
end
