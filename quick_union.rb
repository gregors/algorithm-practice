class QuickUnion
  def initialize(count)
    @ids = (0...count).to_a
  end

  def connected?(q, p)
    root(p) == root(q)
  end

  def union(q, p)
    root_q = root(q)
    root_p = root(p)

    @ids[root_q] = root_p
  end

  def root(n)
    n = @ids[n] while @ids[n] != n
    n
  end
end
