defmodule WeightedQuickUnion do
  def init(count) do
    n = count - 1
    %{ data: Enum.into(0..n, %{}, fn(x) -> {x,  x} end),
       size: Enum.into(0..n, %{}, fn(x) -> {x, 1} end)
    }
  end

  def connect?(data, q, p) do
    root(data, q) == root(data, p)
  end

  def union(data, q, p) do
    root_q = root(data, q)
    root_p = root(data, p) 

    if data[:size][root_q] <= data[:size][root_p] do
      data
        |> put_in([:data, root_q], root_p)
        |> update_size(root_p) 
    else
      data
        |> put_in([:data, root_p], root_q)
        |> update_size(root_q) 
    end
  end

  def root(data, n) do
    if data[:data][n] == n do
      n
    else
      root(data, data[:data][n])
    end
  end

  def update_size(data, n) do
    val = get_in(data, [:size, n]) + 1
    data |> put_in([:size, n], val)
  end
end
