class WeightedQuickUnion
  def initialize(count)
    @ids = (0...count).to_a
    @sz = (0...count).to_a
  end

  def connected?(q, p)
    root(q) == root(p)
  end

  def union(q, p)
    root_q = root(q)
    root_p = root(p)
    
    if @sz[root_q] <= @sz[root_p]
      @ids[root_q] = root_p
      @sz[root_p] += 1
    else
      @ids[root_p] = root_q
      @sz[root_q] += 1
    end
  end

  def root(n)
    n = @ids[n] while @ids[n] != n
    n
  end
end
